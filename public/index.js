const video = document.querySelector('#videoElement');
const photoCanvas = document.querySelector('#photo-canvas');
// const photoDisplay = document.querySelector('#photo');
const stopVidButton = document.querySelector('#stop-vid-btn');
const photoButton = document.querySelector('#photo-btn');

if (navigator.mediaDevices.getUserMedia) {
	console.log(navigator.mediaDevices.getUserMedia);
	navigator.mediaDevices.getUserMedia({ video: true })
		.then(stream => {
			// stream is an object type: MediaStream
			video.srcObject = stream;
		})
		.catch(error => {
			console.error(error);
		});
}

const stopVideo = () => {
	const stream = video.srcObject;
	const tracks = stream.getTracks();
	console.log('stream: ', stream);
	console.log('tracks: ', tracks);
	tracks.forEach(track => track.stop());
}

const takePhoto = () => {
	const context = photoCanvas.getContext('2d');
	context.drawImage(video, 0, 0, photoCanvas.width, photoCanvas.height);
	const data = photoCanvas.toDataURL('image/png');
	// photoDisplay.setAttribute('src', data);
	// photoDisplay.style.display = '';
}

// const clearPhoto = () => {
// 	const context = photoCanvas.getContext('2d');
// 	context.fillStyle = '#AAA';
// 	context.fillRect(0,0, photoCanvas.width, photoCanvas.height);

// 	const data = photoCanvas.toDataURL('image/png');
// 	photoDisplay.setAttribute('src', data);
// }

photoButton.addEventListener('click', (e) => {
	e.preventDefault();
	takePhoto();
})

stopVidButton.addEventListener('click', stopVideo);

// Barcode

if (!('BarcodeDetector' in window)) {
  console.log('Barcode Detector is not supported by this browser.');
  alert('BarcodeDetector not supported')
} else {
  console.log('Barcode Detector supported!');
  alert('Barcode Detector supported');
  // create new detector
  var barcodeDetector = new BarcodeDetector({formats: ['code_39', 'codabar', 'ean_13']});
}

// javascriptBarcodeReader({
//   image: "https://upload.wikimedia.org/wikipedia/en/a/a9/Code_93_wikipedia.png",
//   barcode: "code-93"
// })
//   .then(result => {
//     appDiv.innerHTML = `<h1>${result}</h1>`;
//     console.log(result);
//   })
//   .catch(error => {
//     appDiv.innerHTML = `<h1>${error}</h1>`;
//     console.log(error);
//   });

